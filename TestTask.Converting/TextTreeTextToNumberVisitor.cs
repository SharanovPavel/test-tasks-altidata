﻿using System.Text;
using TestTask.TextTree;

namespace TestTask.Converting;

internal static class TextTreeTextToNumberVisitor
{
    public static string Visit(Text text)
    {
        var builder = new StringBuilder();
        var queue = new Queue<TextNode>(text.Nodes);
        foreach (var node in queue)
        {
            builder.Append(Visit(node));
            if (queue.TryPeek(out var next) && next is WordNode)
            {
                builder.Append(' ');
            }
        }
        return builder.ToString().Trim();
    }

    private static string Visit(TextNode node)
    {
        return node switch
        {
            WordNode word => word.Word,
            PunctuationNode punctuation => VisitPunctuation(punctuation),
            NumberNode number => VisitNumber(number),
            _ => throw new InvalidOperationException($"Unknown node {node.GetType().Name}.")
        };
    }

    private static string VisitNumber(NumberNode node)
    {
        static int MapValue(NumberPart part, int value)
        {
            return part switch
            {
                NumberValue valueNode => value + valueNode.Value,
                NumberQuantifier { Type: NumberQuantifierType.Thousand} => value * 1_000,
                NumberQuantifier { Type: NumberQuantifierType.Million } => value * 1_000_000,
                NumberQuantifier { Type: NumberQuantifierType.Billion } => value * 1_000_000_000,
                _ => throw new InvalidOperationException()
            };
        }
        
        return node.Values.Aggregate(0, (current, part) => MapValue(part, current)).ToString();
    }
    
    private static string VisitPunctuation(PunctuationNode node)
    {
        return node.Type switch
        {
            PunctuationType.Comma => ",",
            PunctuationType.Dot => ".",
            _ => throw new IndexOutOfRangeException()
        };
    }
}