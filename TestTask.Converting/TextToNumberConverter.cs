﻿using TestTask.Parser;

namespace TestTask.Converting;

public static class TextToNumberConverter
{
    public static string Convert(string input)
    {
        var text = TextTreeParser.Parse(input);
        return TextTreeTextToNumberVisitor.Visit(text);
    }
}