module Tests

open FParsec
open TestTask.Parser.FSharp
open TestTask.Parser.FSharp.Converting
open TestTask.Parser.FSharp.Converting.TextTreeConverter
open Xunit
open TextTreeParser


let fail message = Assert.True(false, message)

let assertParser input success =
    let result = parse input
    match result with
        | ParserResult.Success(text, _, _) -> success text
        | ParserResult.Failure(failMessage, _, _) -> fail failMessage

let assertCount expectedCount (source: 'a list)  = Assert.Equal(expectedCount, source.Length) 

[<Theory>]
[<InlineData("Тестовый текст на русском языке без чисел, но со знаками препинания.", 13)>]
[<InlineData("Съешь же ещё этих мягких французских булок, да выпей чаю.", 12)>]
let ``Parse text without numbers`` input expectedNodesCount =
    assertParser input (assertCount expectedNodesCount)
    
[<Theory>]
[<InlineData("сто двадцать пять")>]
[<InlineData("один миллион")>]
[<InlineData("двести миллионов триста сорок восемь тысяч пятьсот пятьдесят семь")>]
let ``Parse text with single number`` input =
    assertParser input (assertCount 1)

[<Fact>]
let ``Parse task text``() =
    let input = "Он заплатил двадцать миллионов за три таких автомобиля"
    let expectedOutput = "Он заплатил 20000000 за 3 таких автомобиля"
    let result = TextTreeConverter.convert input
    match result with
    | Fail message -> Assert.True(false, message)
    | Success actualOutput -> Assert.Equal(expectedOutput, actualOutput)