﻿namespace TestTask.Parser.FSharp.Parsers

open System
open System.Text
open FParsec
open TestTask.Parser.FSharp
open TestTask.Parser.FSharp.TextTree

module internal Common =

    let (+?) x y =
        match y with
        | Some s -> x + s
        | None -> x

    let atLeastOnceString (p: Parser<char, unit>) : Parser<string, unit> =
        many1 p
        >>= fun x ->
                string (List.fold (fun (sb: StringBuilder) (c: char) -> sb.Append(c)) (StringBuilder()) x)
                |> preturn

    let wordWithOptionalEnd firstPart endParts : Parser<string, unit> =
        pipe2
        <| pstring firstPart
        <| opt (choice (Seq.map pstring endParts))
        <| (+?)

    let (&?) = wordWithOptionalEnd

    let pnumber word number : Parser<int, unit> = pstring word >>. preturn number

    let (>==) = pnumber

    let ws: Parser<char, char> = pchar ' '

    let token p = p .>> spaces

module internal Punctuation =

    let str = pstring

    let public dot: Parser<TextTreeElement, unit> =
        str "." >>% Punctuation PunctuationType.Dot

    let public comma: Parser<TextTreeElement, unit> =
        str "," >>% Punctuation PunctuationType.Comma

    let public anyPunctuation = choice [ dot; comma ]

module internal Quantifiers =
    open Common

    let public thousand: Parser<NumberPart, unit> =
        "тысяч" &? [ "а"; "и" ] >>% Quantifier Thousand

    let public million: Parser<NumberPart, unit> =
        "миллион" &? [ "ов"; "ы"; "а" ]
        >>% Quantifier Million

    let public billion: Parser<NumberPart, unit> =
        "миллиард" &? [ "ов"; "а" ] >>% Quantifier Billion

    let public anyQuantifier = choice [ thousand; million; billion ]

module internal Tens =

    let public ten: Parser<NumberPart, unit> = pstring "десять" >>% Value 10
    let public twenty: Parser<NumberPart, unit> = pstring "двадцать" >>% Value 20
    let public thirty: Parser<NumberPart, unit> = pstring "тридцать" >>% Value 30
    let public forty: Parser<NumberPart, unit> = pstring "сорок" >>% Value 40
    let public fifty: Parser<NumberPart, unit> = pstring "пятьдесят" >>% Value 50
    let public sixty: Parser<NumberPart, unit> = pstring "шестьдесят" >>% Value 60
    let public seventy: Parser<NumberPart, unit> = pstring "семьдесят" >>% Value 70
    let public eighty: Parser<NumberPart, unit> = pstring "восемьдесят" >>% Value 80
    let public ninety: Parser<NumberPart, unit> = pstring "девяносто" >>% Value 90

    let public anyTen =
        choice [ ten
                 twenty
                 thirty
                 forty
                 fifty
                 sixty
                 seventy
                 eighty
                 ninety ]

module internal Words =
    open Common

    let public word: Parser<TextTreeElement, unit> = letter |> atLeastOnceString |>> Word

module internal Digits =
    open Common

    let public one = "од" &? [ "ин"; "на" ] >>% Value 1
    let public two = "дв" &? [ "а"; "е" ] >>% Value 2
    let public three: Parser<NumberPart, unit> = pstring "три" >>% Value 3
    let public four: Parser<NumberPart, unit> = pstring "четыре" >>% Value 4
    let public five: Parser<NumberPart, unit> = pstring "пять" >>% Value 5
    let public six: Parser<NumberPart, unit> = pstring "шесть" >>% Value 6
    let public seven: Parser<NumberPart, unit> = pstring "семь" >>% Value 7
    let public eight: Parser<NumberPart, unit> = pstring "восемь" >>% Value 8
    let public nine: Parser<NumberPart, unit> = pstring "девять" >>% Value 9
    let public zero: Parser<NumberPart, unit> = pstring "ноль" >>% Value 0

    let public anyDigit =
        choice [ attempt nine
                 attempt eight
                 attempt seven
                 attempt six
                 attempt five
                 attempt four
                 attempt three
                 attempt two
                 attempt one
                 attempt zero ]

module internal Hundreds =

    let public oneHundred: Parser<NumberPart, unit> = pstring "сто" >>% Value 100
    let public twoHundreds: Parser<NumberPart, unit> = pstring "двести" >>% Value 200
    let public threeHundreds: Parser<NumberPart, unit> = pstring "триста" >>% Value 300
    let public fourHundreds: Parser<NumberPart, unit> = pstring "четыреста" >>% Value 400
    let public fiveHundreds: Parser<NumberPart, unit> = pstring "пятьсот" >>% Value 500
    let public sixHundreds: Parser<NumberPart, unit> = pstring "шестьсот" >>% Value 600
    let public sevenHundreds: Parser<NumberPart, unit> = pstring "семьсот" >>% Value 700
    let public eightHundreds: Parser<NumberPart, unit> = pstring "восемьсот" >>% Value 800
    let public nineHundreds: Parser<NumberPart, unit> = pstring "девятьсот" >>% Value 900

    let public anyHundred =
        choice [ attempt nineHundreds
                 attempt eightHundreds
                 attempt sevenHundreds
                 attempt sixHundreds
                 attempt fiveHundreds
                 attempt fourHundreds
                 attempt threeHundreds
                 attempt twoHundreds
                 attempt oneHundred ]

module internal ElevenToNineteen =
    let suffix: Parser<string, unit> = pstring "надцать"

    let stringWithSuffix str = pipe2 (pstring str) suffix (+)

    let suffixThenNumber word number =
        word |> stringWithSuffix >>% Value number

    let public Eleven = suffixThenNumber "один" 11
    let public Twelve = suffixThenNumber "две" 12
    let public Thirteen = suffixThenNumber "три" 13
    let public Fourteen = suffixThenNumber "четыр" 14
    let public Fifteen = suffixThenNumber "пят" 15
    let public Sixteen = suffixThenNumber "шест" 16
    let public Seventeen = suffixThenNumber "сем" 17
    let public Eighteen = suffixThenNumber "восем" 18
    let public Nineteen = suffixThenNumber "девят" 19

    let public anyElevenToNineteen =
        choice [ attempt Nineteen
                 attempt Eighteen
                 attempt Seventeen
                 attempt Sixteen
                 attempt Fifteen
                 attempt Fourteen
                 attempt Thirteen
                 attempt Twelve
                 attempt Eleven ]

module Nodes =
    open Quantifiers
    open ElevenToNineteen
    open Hundreds
    open Tens
    open Digits
    open Common

    let anyValue =
        choice [ anyHundred
                 anyElevenToNineteen
                 anyTen
                 anyDigit ]

    let numberPart =
        choice [ token anyQuantifier
                 token anyValue ]

    let public numberNode = many1 numberPart |>> Number
