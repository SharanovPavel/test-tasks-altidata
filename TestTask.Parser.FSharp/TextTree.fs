﻿module TestTask.Parser.FSharp.TextTree

type PunctuationType =
    | Comma
    | Dot

type NumberQuantifier =
    | Thousand
    | Million
    | Billion

type NumberPart =
    | Quantifier of NumberQuantifier
    | Value of int

type TextTreeElement =
    | Word of string
    | Punctuation of PunctuationType
    | Number of NumberPart list

type Text = TextTreeElement list