﻿module TestTask.Parser.FSharp.TextTreeParser
open FParsec
open TestTask.Parser.FSharp.Parsers
open TextTree
open Nodes
open Punctuation
open Words
open Common

let private sepBySpaces p = sepEndBy p spaces

let private parser : Parser<Text, unit> = choice [ attempt numberNode; anyPunctuation; word; ] |> sepBySpaces

let public parse input : ParserResult<Text, unit> = run parser input