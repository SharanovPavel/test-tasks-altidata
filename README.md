**sic!** F#-реализация находится в проектах [`TestTask.Parser.FSharp`](https://gitlab.com/SharanovPavel/test-tasks-altidata/-/tree/master/TestTask.Parser.FSharp) и [`TestTask.Parser.FSharp.Converting`](https://gitlab.com/SharanovPavel/test-tasks-altidata/-/tree/master/TestTask.Parser.FSharp.Converting). Unit-тесты находятся в проекте [`TestTask.Parser.FSharp.Tests`](https://gitlab.com/SharanovPavel/test-tasks-altidata/-/tree/master/TestTask.Parser.FSharp.Tests).

В ходе выполнения тестового задания мы не предполагаем что задача, указанная ниже будет решена полностью. Вместо этого, представьте что Вы получили данную задачу и должны как-то начинать работу по ней.

В ходе тестового задания Вы сами решаете сколько времени вы можете и хотите ей посвятить (например 2 часа или 6 часов). Далее Вы начинаете работу по задаче, так, как считаете наиболее эффективным с Вашей точки зрения. По истечении заданного времени Вы собираете все имеющиеся на тот момент результаты, даже неполные, и отправляете нам с указанием того, сколько времени Вы потратили.

Задача:

Написать консольное приложение для конвертации русского текста. На вход поступает произвольный текст, на выходе должна получаться копия текста, но числа, записанные словами, должны заменяться на эквивалентные числовые представления. Пример:

Вход: "Он заплатил двадцать миллионов за три таких автомобиля"

Выход: "Он заплатил 20000000 за 3 таких автомобиля"
