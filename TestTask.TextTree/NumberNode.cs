﻿using System.Collections.Immutable;

namespace TestTask.TextTree;

public class NumberNode : TextNode
{
    public ImmutableList<NumberPart> Values { get; }
    
    public NumberNode(IEnumerable<NumberPart> numberParts)
    {
        Values = numberParts.ToImmutableList();
    }
}