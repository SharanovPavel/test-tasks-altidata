﻿namespace TestTask.TextTree;

public enum NumberQuantifierType
{
    None,
    Thousand,
    Million,
    Billion
}