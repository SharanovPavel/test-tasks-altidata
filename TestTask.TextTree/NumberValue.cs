namespace TestTask.TextTree;

public class NumberValue : NumberPart
{
    public int Value { get; }

    public NumberValue(int value)
    {
        Value = value;
    }
}