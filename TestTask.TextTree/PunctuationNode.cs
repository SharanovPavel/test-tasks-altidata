﻿namespace TestTask.TextTree;

public class PunctuationNode : TextNode
{
    public PunctuationType Type { get; }

    public PunctuationNode(PunctuationType type)
    {
        Type = type;
    }
}

public enum PunctuationType
{
    Comma,
    Dot
}