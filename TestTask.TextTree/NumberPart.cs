﻿namespace TestTask.TextTree;

public abstract class NumberPart
{
}

public class NumberQuantifier : NumberPart
{
    public NumberQuantifierType Type { get; }

    public NumberQuantifier(NumberQuantifierType type)
    {
        Type = type;
    }
}

