﻿using System.Collections.Immutable;

namespace TestTask.TextTree;

public class Text
{
    public ImmutableList<TextNode> Nodes { get; }
    
    public Text(IEnumerable<TextNode> nodes)
    {
        Nodes = nodes.ToImmutableList();
    }
}