﻿namespace TestTask.TextTree;

public class WordNode : TextNode
{
    public string Word { get; }

    public WordNode(string word)
    {
        Word = word;
    }
}