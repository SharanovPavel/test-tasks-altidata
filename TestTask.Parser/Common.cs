﻿namespace TestTask.Parser;

using static Pidgin.Parser;
using Pidgin;

public static class Common
{
    public static Parser<char, T> Token<T>(Parser<char, T> token)
        => Try(token).Before(SkipWhitespaces);
    
    public static Parser<char, string> Token(string token)
        => Token(String(token));
}