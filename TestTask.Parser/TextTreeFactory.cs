﻿using TestTask.TextTree;

namespace TestTask.Parser;

public static class TextTreeFactory
{
    public static Text Text(IEnumerable<TextNode> nodes) => new (nodes);
    public static TextNode Word(string word) => new WordNode(word);
    public static TextNode NumberNode(IEnumerable<NumberPart> parts) => new NumberNode(parts);
    public static NumberPart NumberValue(int value) =>
        new NumberValue(value);

    public static NumberPart NumberQuantifier(NumberQuantifierType type)
        => new NumberQuantifier(type);
    
    public static TextNode Punctuation(PunctuationType type) => new PunctuationNode(type);
    public static TextNode Comma() => Punctuation(PunctuationType.Comma);
    public static TextNode Dot() => Punctuation(PunctuationType.Dot);
}