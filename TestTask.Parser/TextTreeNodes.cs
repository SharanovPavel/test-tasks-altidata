﻿using TestTask.Parser.TextParsers;
using TestTask.TextTree;

namespace TestTask.Parser;

using Pidgin;
using static Pidgin.Parser;
using static Words;
using static Punctuation;
using static QuantifierWords;
using static WordToValue;
using static Common;

public static class TextTreeNodes
{
    public static Parser<char, TextNode> CommaNode = Comma.ThenReturn(TextTreeFactory.Comma());
    public static Parser<char, TextNode> DotNode = Dot.ThenReturn(TextTreeFactory.Dot());

    public static Parser<char, NumberQuantifierType> ThousandQuantifier =
        Thousand.ThenReturn(NumberQuantifierType.Thousand);
    public static Parser<char, NumberQuantifierType> MillionQuantifier =
        Million.ThenReturn(NumberQuantifierType.Million);
    public static Parser<char, NumberQuantifierType> BillionQuantifier =
        Billion.ThenReturn(NumberQuantifierType.Billion);

    public static Parser<char, NumberQuantifierType> AnyQuantifier = OneOf(
        ThousandQuantifier,
        MillionQuantifier,
        BillionQuantifier);

    public static Parser<char, NumberPart> NumberValuePart = OneOf(
        Token(AnyValue).Map(TextTreeFactory.NumberValue),
        Token(AnyQuantifier).Map(TextTreeFactory.NumberQuantifier));

    public static Parser<char, TextNode> WordNode = Word.Map(TextTreeFactory.Word);
    public static Parser<char, TextNode> PunctuationNode = OneOf(DotNode, CommaNode);

    public static Parser<char, TextNode>
        NumberNode = Try(NumberValuePart).AtLeastOnce().Map(TextTreeFactory.NumberNode);
}