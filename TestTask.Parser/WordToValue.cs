﻿using Pidgin;

using static TestTask.Parser.TextParsers.DigitWords;
using static TestTask.Parser.TextParsers.TenWords;
using static TestTask.Parser.TextParsers.ElevenToNineteenWords;
using static TestTask.Parser.TextParsers.HundredWords;
using static Pidgin.Parser;

namespace TestTask.Parser;

public static class WordToValue
{
    public static Parser<char, int> ValueZero = WordZero.ThenReturn(0);
    public static Parser<char, int> ValueOne = WordOne.ThenReturn(1);
    public static Parser<char, int> ValueTwo = WordTwo.ThenReturn(2);
    public static Parser<char, int> ValueThree = WordThree.ThenReturn(3);
    public static Parser<char, int> ValueFour = WordFour.ThenReturn(4);
    public static Parser<char, int> ValueFive = WordFive.ThenReturn(5);
    public static Parser<char, int> ValueSix = WordSix.ThenReturn(6);
    public static Parser<char, int> ValueSeven = WordSeven.ThenReturn(7);
    public static Parser<char, int> ValueEight = WordEight.ThenReturn(8);
    public static Parser<char, int> ValueNine = WordNine.ThenReturn(9);
    public static Parser<char, int> ValueTen = WordTen.ThenReturn(10);
    public static Parser<char, int> ValueEleven = WordEleven.ThenReturn(11);
    public static Parser<char, int> ValueTwelve = WordTwelve.ThenReturn(12);
    public static Parser<char, int> ValueThirteen = WordThirteen.ThenReturn(13);
    public static Parser<char, int> ValueFourteen = WordFourteen.ThenReturn(14);
    public static Parser<char, int> ValueFifteen = WordFifteen.ThenReturn(15);
    public static Parser<char, int> ValueSixteen = WordSixteen.ThenReturn(16);
    public static Parser<char, int> ValueSeventeen = WordSeventeen.ThenReturn(17);
    public static Parser<char, int> ValueEighteen = WordEighteen.ThenReturn(18);
    public static Parser<char, int> ValueNineteen = WordNineteen.ThenReturn(19);
    public static Parser<char, int> ValueTwenty = WordTwenty.ThenReturn(20);
    public static Parser<char, int> ValueThirty = WordThirty.ThenReturn(30);
    public static Parser<char, int> ValueForty = WordForty.ThenReturn(40);
    public static Parser<char, int> ValueFifty = WordFifty.ThenReturn(50);
    public static Parser<char, int> ValueSixty = WordSixty.ThenReturn(60);
    public static Parser<char, int> ValueSeventy = WordSeventy.ThenReturn(70);
    public static Parser<char, int> ValueEighty = WordEighty.ThenReturn(80);
    public static Parser<char, int> ValueNinety = WordNinety.ThenReturn(90);
    public static Parser<char, int> ValueOneHundred = WordOneHundred.ThenReturn(100);
    public static Parser<char, int> ValueTwoHundreds = WordTwoHundreds.ThenReturn(200);
    public static Parser<char, int> ValueThreeHundreds = WordThreeHundreds.ThenReturn(300);
    public static Parser<char, int> ValueFourHundreds = WordFourHundreds.ThenReturn(400);
    public static Parser<char, int> ValueFiveHundreds = WordFiveHundreds.ThenReturn(500);
    public static Parser<char, int> ValueSixHundreds = WordSixHundreds.ThenReturn(600);
    public static Parser<char, int> ValueSevenHundreds = WordSevenHundreds.ThenReturn(700);
    public static Parser<char, int> ValueEightHundreds = WordEightHundreds.ThenReturn(800);
    public static Parser<char, int> ValueNineHundreds = WordNineHundreds.ThenReturn(900);

    public static Parser<char, int> AnyValue = OneOf(
        Try(ValueNineHundreds),
        Try(ValueEightHundreds),
        Try(ValueSevenHundreds),
        Try(ValueSixHundreds),
        Try(ValueFiveHundreds),
        Try(ValueFourHundreds),
        Try(ValueThreeHundreds),
        Try(ValueTwoHundreds),
        Try(ValueOneHundred),
        Try(ValueNineteen),
        Try(ValueEighteen),
        Try(ValueSeventeen),
        Try(ValueSixteen),
        Try(ValueFifteen),
        Try(ValueFourteen),
        Try(ValueThirteen),
        Try(ValueTwelve),
        Try(ValueEleven),
        Try(ValueNinety),
        Try(ValueEighty),
        Try(ValueSeventy),
        Try(ValueSixty),
        Try(ValueFifty),
        Try(ValueForty),
        Try(ValueThirty),
        Try(ValueTwenty),
        Try(ValueTen),
        Try(ValueNine),
        Try(ValueEight),
        Try(ValueSeven),
        Try(ValueSix),
        Try(ValueFive),
        Try(ValueFour),
        Try(ValueThree),
        Try(ValueTwo),
        Try(ValueOne),
        Try(ValueZero));
}