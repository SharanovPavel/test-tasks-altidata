using Pidgin;
using static Pidgin.Parser;
using String = System.String;

namespace TestTask.Parser.TextParsers;
public static class DigitWords
{
    public static readonly Parser<char, string> WordOne =
        from begin in String("од")
            from end in OneOf(String("ин"), String("на"))
            select begin + end;

    public static readonly Parser<char, string> WordTwo =
        from begin in String("дв")
        from end in OneOf('а', 'е')
        select begin + end;
    
    public static readonly Parser<char, string> WordThree = String("три");
    public static readonly Parser<char, string> WordFour = String("четыре");
    public static readonly Parser<char, string> WordFive = String("пять");
    public static readonly Parser<char, string> WordSix = String("шесть");
    public static readonly Parser<char, string> WordSeven = String("семь");
    public static readonly Parser<char, string> WordEight = String("восемь");
    public static readonly Parser<char, string> WordNine = String("девять");
    public static readonly Parser<char, string> WordZero = String("ноль");
}