﻿using Pidgin;

namespace TestTask.Parser.TextParsers;

using static Pidgin.Parser;

public static class Words
{
    public static Parser<char, string> Word = Letter.AtLeastOnceString();
}