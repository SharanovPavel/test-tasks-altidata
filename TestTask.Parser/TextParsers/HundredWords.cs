﻿namespace TestTask.Parser.TextParsers;

using Pidgin;
using static Pidgin.Parser;

public static class HundredWords
{
    public static Parser<char, string> WordOneHundred = String("сто");
    public static Parser<char, string> WordTwoHundreds = String("двести");
    public static Parser<char, string> WordThreeHundreds = String("триста");
    public static Parser<char, string> WordFourHundreds = String("четыреста");
    public static Parser<char, string> WordFiveHundreds = String("пятьсот");
    public static Parser<char, string> WordSixHundreds = String("шестьсот");
    public static Parser<char, string> WordSevenHundreds = String("семьсот");
    public static Parser<char, string> WordEightHundreds = String("восемьсот");
    public static Parser<char, string> WordNineHundreds = String("девятьсот");
    
}