﻿using Pidgin;

namespace TestTask.Parser.TextParsers;

using static Pidgin.Parser;

public static class TenWords
{
    public static Parser<char, string> WordTen = String("десять");
    public static Parser<char, string> WordTwenty = String("двадцать");
    public static Parser<char, string> WordThirty = String("тридцать");
    public static Parser<char, string> WordForty = String("сорок");
    public static Parser<char, string> WordFifty = String("пятьдесят");
    public static Parser<char, string> WordSixty = String("шестьдесят");
    public static Parser<char, string> WordSeventy = String("семьдесят");
    public static Parser<char, string> WordEighty = String("восемьдесят");
    public static Parser<char, string> WordNinety = String("девяносто");
    
}