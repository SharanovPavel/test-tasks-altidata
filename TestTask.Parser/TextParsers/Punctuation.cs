﻿using Pidgin;
using static Pidgin.Parser;

namespace TestTask.Parser.TextParsers;

public static class Punctuation
{
    public static Parser<char, char> Comma = Char(',');
    public static Parser<char, char> Dot = Char('.');
}