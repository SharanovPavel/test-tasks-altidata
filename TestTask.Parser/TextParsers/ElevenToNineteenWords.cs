﻿using Pidgin;
using static Pidgin.Parser;
using String = System.String;

namespace TestTask.Parser.TextParsers;

public static class ElevenToNineteenWords
{
    private static Parser<char, string> Suffix = String("надцать");

    private static Parser<char, string> StringWithSuffix(string input) =>
        String(input).Then(Suffix, (begin, suffix) => begin + suffix);

    public static Parser<char, string> WordEleven = StringWithSuffix("один");
    public static Parser<char, string> WordTwelve = StringWithSuffix("две");
    public static Parser<char, string> WordThirteen = StringWithSuffix("три");
    public static Parser<char, string> WordFourteen = StringWithSuffix("четыр");
    public static Parser<char, string> WordFifteen = StringWithSuffix("пят");
    public static Parser<char, string> WordSixteen = StringWithSuffix("шест");
    public static Parser<char, string> WordSeventeen = StringWithSuffix("сем");
    public static Parser<char, string> WordEighteen = StringWithSuffix("восем");
    public static Parser<char, string> WordNineteen = StringWithSuffix("девят");
}