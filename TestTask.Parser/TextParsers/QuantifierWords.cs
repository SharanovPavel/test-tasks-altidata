﻿using Pidgin;

namespace TestTask.Parser.TextParsers;

using static Pidgin.Parser;

public static class QuantifierWords
{
    public static Parser<char, string> Thousand =
        from begin in String("тысяч")
        from end in OneOf('а', 'и').Optional()
        select begin + end;

    public static Parser<char, string> Million =
        from begin in String("миллион")
        from end in OneOf(String("ов"), String("ы"), String("а")).Optional()
        select begin + end;

    public static Parser<char, string> Billion =
        from begin in String("миллиард")
        from end in OneOf(String("ов"), String("а")).Optional()
        select begin + end;
}