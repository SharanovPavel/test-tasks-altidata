﻿using Pidgin;

using static TestTask.Parser.TextTreeNodes;
using static Pidgin.Parser;

namespace TestTask.Parser;

public static class TextTreeParser
{
    private static readonly Parser<char, TextTree.Text> _parser =
        Try(NumberNode).Or(OneOf(WordNode, PunctuationNode)).SeparatedAtLeastOnce(Whitespace.Optional())
            .Map(TextTreeFactory.Text);

    public static TextTree.Text Parse(string input)
    {
        return _parser.ParseOrThrow(input);
    }
}