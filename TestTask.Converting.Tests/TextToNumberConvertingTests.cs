﻿using System.Collections.Generic;
using FluentAssertions;
using Xunit;

namespace TestTask.Converting.Tests;

public class TextToNumberConvertingTests
{
    [Theory]
    [MemberData(nameof(Converting_text_with_words_to_text_with_digits_data))]
    public void Converting_text_with_words_to_text_with_digits(string input, string expected)
    {
        var actual = TextToNumberConverter.Convert(input);
        actual.Should().Be(expected);
    }

    public static IEnumerable<object[]> Converting_text_with_words_to_text_with_digits_data()
    {
        return new[]
        {
            new object[]
            {
                "Он заплатил двадцать миллионов за три таких автомобиля", 
                "Он заплатил 20000000 за 3 таких автомобиля"
            }
        };
    }
}