﻿using FluentAssertions;
using Xunit;

namespace TestTask.Parser.Tests;

public class TextTreeParserTests
{
    [Theory]
    [InlineData("Тестовый текст на русском языке без чисел, но со знаками препинания.", 13)]
    [InlineData("Съешь же ещё этих мягких французских булок, да выпей чаю.", 12)]
    public void Text_without_numbers_parsing(string input, int expectedNodesCount)
    {
        var text = TextTreeParser.Parse(input);
        text.Nodes.Count.Should().Be(expectedNodesCount, $"текст имеет {expectedNodesCount} нод.");
    }

    [Theory]
    [InlineData("сто двадцать пять")]
    [InlineData("двести тридцать семь")]
    [InlineData("один миллион")]
    [InlineData("двести миллионов триста сорок восемь тысяч пятьсот пятьдесят семь")]
    public void Single_number_words_parsing(string input)
    {
        var text = TextTreeParser.Parse(input);
        text.Nodes.Should().HaveCount(1);
    }

    [Theory]
    [InlineData("ноль без палочки.", 4)]
    [InlineData("один раз отмерь, семь раз отрежь.", 8)]
    [InlineData("семь тысяч триста пятьдесят две штуки.", 3)]
    public void Text_with_numbers_parsing(string input, int expectedNodesCount)
    {
        var text = TextTreeParser.Parse(input);
        text.Nodes.Count.Should().Be(expectedNodesCount, $"текст имеет {expectedNodesCount} нод.");
    }
}