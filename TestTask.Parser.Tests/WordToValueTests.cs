﻿using System.Collections.Generic;
using FluentAssertions;
using Pidgin;
using Xunit;

namespace TestTask.Parser.Tests;

public class WordToValueTests
{
    [Theory]
    [MemberData(nameof(Single_word_numbers_parsing_data))]
    public void Single_word_numbers_parsing(string input, int expected)
    {
        var actual = WordToValue.AnyValue.ParseOrThrow(input);
        actual.Should().Be(expected);
    }

    public static IEnumerable<object[]> Single_word_numbers_parsing_data()
    {
        return new[]
        {
            new object[] { "один", 1 },
            new object[] { "два", 2 },
            new object[] { "три", 3 },
            new object[] { "четыре", 4 },
            new object[] { "пять", 5 },
            new object[] { "шесть", 6 },
            new object[] { "семь", 7 },
            new object[] { "восемь", 8 },
            new object[] { "девять", 9 },
            new object[] { "десять", 10 },
            new object[] { "одиннадцать", 11 },
            new object[] { "двенадцать", 12 },
            new object[] { "тринадцать", 13 },
            new object[] { "четырнадцать", 14 },
            new object[] { "пятнадцать", 15 },
            new object[] { "шестнадцать", 16 },
            new object[] { "семнадцать", 17 },
            new object[] { "восемнадцать", 18 },
            new object[] { "девятнадцать", 19 },
            new object[] { "двадцать", 20 },
            new object[] { "сто", 100 },
            new object[] { "двести", 200 },
            new object[] { "триста", 300 },
            new object[] { "четыреста", 400 },
            new object[] { "пятьсот", 500 },
            new object[] { "шестьсот", 600 },
            new object[] { "семьсот", 700 },
            new object[] { "восемьсот", 800 },
            new object[] { "девятьсот", 900 }
        };
    }
}