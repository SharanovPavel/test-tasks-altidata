﻿module TestTask.Parser.FSharp.Converting.TextTreeConverter

open System
open System.Collections.Generic
open System.Text
open TestTask.Parser.FSharp
open FParsec
open TestTask.Parser.FSharp.TextTree

type ConvertResult =
    | Success of string
    | Fail of string

let private mapNumberValue value part =
    match part with
    | Value i -> value + i
    | Quantifier x when x = NumberQuantifier.Thousand -> value * 1_000
    | Quantifier x when x = NumberQuantifier.Million -> value * 1_000_000
    | Quantifier x when x = NumberQuantifier.Billion -> value * 1_000_000_000
    | _ -> value

let private visitNode node =
    match node with
        | Word word -> word
        | Punctuation punctuationType -> match punctuationType with
                                            | Comma -> ","
                                            | Dot -> "."
        | Number parts -> List.fold mapNumberValue 0 parts |> string
        
let private visitText (text:Text) =
    let mutable builder = StringBuilder()
    let queue = Queue text
    for node in queue do
        builder <- visitNode node |> builder.Append
        let success, next = queue.TryPeek()
        if success then
            match next with
            | Word _ -> builder <- builder.Append(' ')
            | _ -> ()        
    builder.ToString().Trim()

let convert input =
    (TextTreeParser.parse input)
    |> function
        | ParserResult.Success(text, _, _) -> visitText text |> ConvertResult.Success
        | ParserResult.Failure(failMessage, _, _) -> failMessage |> ConvertResult.Fail
